//
//  ExpensifyAPITests.m
//  Expensified
//
//  Created by Larry Borsato on 2015-02-21.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "ExpensifyAPI.h"

@interface ExpensifyAPITests : XCTestCase

@end

@implementation ExpensifyAPITests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testAuthenticationPerformance {
    // This is an example of a performance test case.
    [self measureBlock:^{
		[[ExpensifyAPI sharedInstance] authenticateWithEmail:@"expensifytest@mailinator.com"
											        password:@"hire_me"];
    }];
}

@end
