//
//  LoginViewControllerTests.m
//  Expensified
//
//  Created by Larry Borsato on 2015-02-19.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "LoginViewController.h"

@interface LoginViewControllerTests : XCTestCase

@property (strong, nonatomic)	LoginViewController	*vc;

@end

@implementation LoginViewControllerTests

- (void)setUp {
    [super setUp];

	self.vc = [[LoginViewController alloc] init];
	[self.vc view];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


- (void)testEmailShouldNotBeBlank
{
	self.vc.textFieldEmail.text = @"";
	XCTAssertFalse([self.vc isEmailAddressValid], @"Blank email passed.");
}


- (void)testInvalidEmailShouldFail
{
	self.vc.textFieldEmail.text = @"test.com";
	XCTAssertFalse([self.vc isEmailAddressValid], @"Invalid email passed.");
}


- (void)testValidEmailShouldSucceed
{
	self.vc.textFieldEmail.text = @"test@test.com";
	XCTAssertTrue([self.vc isEmailAddressValid], @"Valid email failed.");
}


- (void)testPasswordShouldNotBeBlank
{
	self.vc.textFieldPassword.text = @"";
	XCTAssertFalse([self.vc isPasswordValid], @"Blank password passed.");
}


- (void)testValidCredentialsShouldPass
{
	self.vc.textFieldEmail.text		= @"test@test.com";
	self.vc.textFieldPassword.text	= @"test";
	XCTAssertTrue([self.vc validateCredentials], @"Valid credentials failed.");
}


- (void)testInvalidCredentialsShouldFail
{
	self.vc.textFieldEmail.text		= @"testtest.com";
	self.vc.textFieldPassword.text	= @"";
	XCTAssertFalse([self.vc validateCredentials], @"Invalid credentials passed.");
}


- (void)testBlankCredentialsShouldFail
{
	self.vc.textFieldEmail.text		= @"";
	self.vc.textFieldPassword.text	= @"";
	XCTAssertFalse([self.vc validateCredentials], @"Blank credentials passed.");
}



@end
