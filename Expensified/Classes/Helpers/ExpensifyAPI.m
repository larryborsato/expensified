//
//  ExpensifyAPI.m
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import "ExpensifyAPI.h"
#import "Constants.h"
#import "SVProgressHUD.h"

@implementation ExpensifyAPI

static ExpensifyAPI *sharedInstance = nil;


+ (ExpensifyAPI *) sharedInstance {
	@synchronized(self) {
		if (sharedInstance == nil) {
			sharedInstance = [[ExpensifyAPI alloc] init];
		}
	}
	return sharedInstance;
}


+ (id) allocWithZone:(NSZone *) zone {
	@synchronized(self) {
		if (sharedInstance == nil) {
			sharedInstance = [super allocWithZone:zone];
			return sharedInstance;
		}
	}
	return nil;
}



- (id)init {
	if ((self = [super init])) {
		self.statusMessages = @{
								[NSNumber numberWithLong:kExpensifyStatusOK]:						@"OK",
								[NSNumber numberWithLong:kExpensifyStatusUnrecognizedCommand]:		@"UnrecgnizedCommand",
								[NSNumber numberWithLong:kExpensifyStatusIncorrectPassword]:		@"IncorrectPassword",
								[NSNumber numberWithLong:kExpensifyStatusMissingArgument]:			@"MissingArgument",
								[NSNumber numberWithLong:kExpensifyStatusAccountNotFound]:			@"AccountNotFound",
								[NSNumber numberWithLong:kExpensifyStatusEmailNotValidated]:		@"EmailNotValidated",
								[NSNumber numberWithLong:kExpensifyStatusMalformedToken]:			@"MalformedToken",
								[NSNumber numberWithLong:kExpensifyStatusTokenExpired]:				@"TokenExpired",
								[NSNumber numberWithLong:kExpensifyStatusInsufficientPrivileges]:	@"InsufficientPrivileges",
								[NSNumber numberWithLong:kExpensifyStatusAborted]:					@"Aborted",
								[NSNumber numberWithLong:kExpensifyStatusDBTransactionError]:		@"DBTransactionError",
								[NSNumber numberWithLong:kExpensifyStatusQueryError]:				@"QueryError",
								[NSNumber numberWithLong:kExpensifyStatusQueryResponseError]:		@"QueryResponseError",
								[NSNumber numberWithLong:kExpensifyStatusUnrecognizedObjectState]:	@"UnrecognizedObjectState"
								};
	}
	return self;
}


/**
 *	Clear the known credentials
 */
- (void) clearCredentials
{
	self.email 		= nil;
	self.password	= nil;
	self.authToken	= nil;
}


/**
 *	Authenticate using the provided email and password credentials
 *
 *	@param	NSString*	the email
 *	@param	NSString*	the password
 *
 *	@return	NSString*	the auth token
 */
- (NSString *) authenticateWithEmail:(NSString *)email password:(NSString *)password
{
	self.email 		= email;
	self.password	= password;
	return [self authenticate];
}

	
/**
 *	Authenticate using the known credentials synchronously
 *
 *	@return	NSString*	the auth token
 */
- (NSString *) authenticate
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD showWithStatus:NSLocalizedString(@"Authenticating", @"authenticating")];
	});
	
	NSString *requestUrl = [NSString stringWithFormat:(NSString *)kExpensifyAuthenticate,
							kExpensifyPartnerName,
							kExpensifyPartnerPassword,
							self.email,
							self.password];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
	[request setURL:[NSURL URLWithString:requestUrl]];
	[request setHTTPMethod:@"GET"];
	[request setTimeoutInterval:30];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	NSHTTPURLResponse 	*response = nil;
	NSError 		*error = nil;
	NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	dispatch_async(dispatch_get_main_queue(), ^{
		[SVProgressHUD dismiss];
	});
	
	if ( response.statusCode == kHttpStatusCodeOK )
	{
		NSError	*jsonError = nil;
		NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
															 options:NSJSONReadingMutableContainers
															   error:&jsonError];
		if ( !jsonError )
		{
			NSInteger jsonCode = [json[kExpensifyKeyJsonCode] integerValue];
			switch ( jsonCode )
			{
				case kExpensifyStatusOK:
				{
					self.authToken 	= json[kExpensifyKeyAuthToken];
					return self.authToken;
					break;
				}
				default:
				{
					[[ExpensifyAPI sharedInstance] showJsonErrorWarning:jsonCode];
					break;
				}
			}
		}
	}
	else
	{
		[[ExpensifyAPI sharedInstance] showNetworkErrorWarning:((NSHTTPURLResponse*)response).statusCode];
	}
	return nil;
}


/**
 *	Authenticate using the known credentials using a block handler
 *
 *	@param	HttpRequestCompletedHandler		the handler to be used at completion
 */
- (void) authenticateWithHandler:(HttpRequestCompletedHandler)handler
{
	NSString *requestUrl = [NSString stringWithFormat:(NSString *)kExpensifyAuthenticate,
							kExpensifyPartnerName,
							kExpensifyPartnerPassword,
							self.email,
							self.password];
	[self httpGetRequestWithUrl:[requestUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] handler:handler];
}


/**
 *	Get the list of transactions using a block handler
 *
 *	@param	HttpRequestCompletedHandler		the handler to be used at completion
 */
- (void) getTransactionsWithHandler:(HttpRequestCompletedHandler)handler
{
	NSString *requestUrl = [NSString stringWithFormat:(NSString *)kExpensifyGet,
							self.authToken,
							kExpensifyReturnListTypeTransactionList];
	[self httpGetRequestWithUrl:[requestUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] handler:handler];
}


/**
 *	Create a transaction a block handler
 *
 *	@param	NSString*						the date of the transaction
 *	@param	long							the amount in pennies
 *	@param	NSString*						the merchant name
 *	@param	HttpRequestCompletedHandler		the handler to be used at completion
 */
- (void) createTransactionsWithDate:(NSString *)created
							 amount:(long)amount
						   merchant:(NSString *)merchant
                            handler:(HttpRequestCompletedHandler)handler
{
	NSString *requestUrl = [NSString stringWithFormat:(NSString *)kExpensifyCreateTransaction,
							self.authToken,
							created,
							amount,
							kExpensifyCurrencyUSD,
							merchant];
	[self httpGetRequestWithUrl:[requestUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] handler:handler];
}


/**
 *	Perform an HTTP GET request using a block handler
 *
 *	@param	NSString*						teh request url
 *	@param	HttpRequestCompletedHandler		the handler to be used at completion
 */
- (void) httpGetRequestWithUrl:(NSString *)urlString handler:(HttpRequestCompletedHandler)handler
{
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
														   cachePolicy:NSURLRequestUseProtocolCachePolicy
													   timeoutInterval:10];
	[request setHTTPMethod:@"GET"];
	NSOperationQueue *queue = [[NSOperationQueue alloc] init];
	[NSURLConnection sendAsynchronousRequest:request queue:queue
						   completionHandler:handler];
}


/**
 *	Do an HTTP GET request and return the results
 *
 *	@param	NSString *	url		the URL
 *
 *	@return	NSDictionary *		a dictionary containing the response, error, and data
 */
- (NSDictionary *)httpGetRequestWithUrl:(NSString *)url
{
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
	[request setURL:[NSURL URLWithString:url]];
	[request setHTTPMethod:@"GET"];
	[request setTimeoutInterval:30];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	NSHTTPURLResponse 	*response = nil;
	NSError 		*error = nil;
	NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSMutableDictionary *rc = [[NSMutableDictionary alloc] init];
	if ( response )
		[rc setObject:response forKey:@"response"];
	if ( error )
		[rc setObject:error forKey:@"error"];
	if ( data )
		[rc setObject:data forKey:@"data"];
	
	return (NSDictionary *)rc;
}


/**
 *	Do an HTTP POST request and return the results
 *
 *	@param	NSString 	 *	url		the URL
 *	@param	NSDictionary *	params	the POST params
 *
 *	@return	NSDictionary *		a dictionary containing the response, error, and data
 */
- (NSDictionary *)httpPostRequestWithUrl:(NSString *)url params:(NSDictionary *)params
{
	NSString *postBody = @"";
	for ( NSString *key in [params allKeys] )
	{
		if ( [postBody length] == 0 )
			postBody = [postBody stringByAppendingFormat:@"%@=%@", key, params[key]];
		else
			postBody = [postBody stringByAppendingFormat:@"&%@=%@", key, params[key]];
	}
	postBody = [postBody stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSData *postBodyData = [postBody dataUsingEncoding:NSUTF8StringEncoding];
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
	[request setURL:[NSURL URLWithString:url]];
	[request setHTTPMethod:@"POST"];
	[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	[request setTimeoutInterval:30];
	[request setHTTPBody:postBodyData];
	[request setValue:[NSString stringWithFormat:@"%ld", (long)[postBodyData length]] forHTTPHeaderField:@"Content-Length"];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	NSHTTPURLResponse 	*response = nil;
	NSError 		*error = nil;
	NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSMutableDictionary *rc = [[NSMutableDictionary alloc] init];
	if ( response )
		[rc setObject:response forKey:@"response"];
	if ( error )
		[rc setObject:error forKey:@"error"];
	if ( data )
		[rc setObject:data forKey:@"data"];
	
	return (NSDictionary *)rc;
}


/**
 *	Show an HTTP network warning
 *	@param	NSInteger		the HTTP status code
 */
- (void) showNetworkErrorWarning:(NSInteger)httpStatusCode
{
	NSString *message = [NSString stringWithFormat:@"A network error was returned (%ld: %@). Please try again",
						 (long)httpStatusCode,
						 [NSHTTPURLResponse localizedStringForStatusCode:httpStatusCode]
						 ];
	[self showWarning:message];
}


/**
 *	Show an Expensify API warning
 *	@param	NSInteger		the json status code
 */
- (void) showJsonErrorWarning:(NSInteger)jsonCode
{
	NSString *message = [NSString stringWithFormat:@"The Expensify server returned an error (%ld - %@). Please try again",
						 (long)jsonCode,
						 self.statusMessages[[NSNumber numberWithLong:jsonCode]]];
	[self showWarning:message];
}


/**
 *	Show a bad JSON warning
 */
- (void) showJsonInvalidWarning
{
	NSString *message = @"Unable to parse response from server";
	[self showWarning:message];
}


/**
 *	Show a warning
 *
 *	@param	NSString*	the message
 */
- (void) showWarning:(NSString *)message
{
	dispatch_async(dispatch_get_main_queue(),^ {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"Warning")
															message:NSLocalizedString(message, @"the message")
														   delegate:self
												  cancelButtonTitle:NSLocalizedString(@"OK", "Cancel button")
												  otherButtonTitles:nil];
		[alertView show];
	});
}


@end
