//
//  ExpensifyAPI.h
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^HttpRequestCompletedHandler) (NSURLResponse *response, NSData *data, NSError *error);

static const NSString	*kExpensifyPartnerName		= @"applicant";
static const NSString	*kExpensifyPartnerPassword	= @"d7c3119c6cdab02d68d9";

static const NSString	*kExpensifyKeyAuthToken			=	@"authToken";
static const NSString	*kExpensifyKeyJsonCode			=	@"jsonCode";
static const NSString	*kExpensifyKeyMessage			=	@"message";
static const NSString	*kExpensifyKeyTransactionList	= 	@"transactionList";
static const NSString	*kExpensifyKeyCreated			=	@"created";
static const NSString	*kExpensifyKeyMerchant			=	@"merchant";
static const NSString	*kExpensifyKeyAmount			=	@"amount";
static const NSString	*kExpensifyKeyCurrency			= 	@"currency";
static const NSString	*kExpensifyKeyCategory			= 	@"category";
static const NSString	*kExpensifyKeyReportID			= 	@"reportID";

static const NSString	*kExpensifyReturnListTypeTransactionList =	@"transactionList";

static const NSString   *kExpensifyCurrencyUSD			= 	@"USD";

static const NSString	*kExpensifyAuthenticate		= @"https://api.expensify.com?command=Authenticate&partnerName=%@&partnerPassword=%@&partnerUserID=%@&partnerUserSecret=%@&useExpensifyLogin=false";

static const NSString	*kExpensifyGet = @"https://api.expensify.com?command=Get&authToken=%@&returnValueList=%@";

static const NSString	*kExpensifyCreateTransaction = @"https://api.expensify.com?command=CreateTransaction&authToken=%@&created=%@&amount=%ld&currency=%@&merchant=%@";

static const NSUInteger kExpensifyStatusOK						=	200;
static const NSUInteger kExpensifyStatusUnrecognizedCommand		=	400;
static const NSUInteger kExpensifyStatusIncorrectPassword		=	401;
static const NSUInteger kExpensifyStatusMissingArgument			=	402;
static const NSUInteger kExpensifyStatusAccountNotFound			=	404;
static const NSUInteger kExpensifyStatusEmailNotValidated		=	405;
static const NSUInteger kExpensifyStatusMalformedToken			=	407;
static const NSUInteger kExpensifyStatusTokenExpired			=	408;
static const NSUInteger kExpensifyStatusInsufficientPrivileges	=	411;
static const NSUInteger kExpensifyStatusAborted					=	500;
static const NSUInteger kExpensifyStatusDBTransactionError		=	501;
static const NSUInteger kExpensifyStatusQueryError				=	502;
static const NSUInteger kExpensifyStatusQueryResponseError		=	503;
static const NSUInteger kExpensifyStatusUnrecognizedObjectState	=	504;


@interface ExpensifyAPI : NSObject

@property	NSDictionary	*statusMessages;
@property	NSString		*email;
@property	NSString		*password;
@property	NSString		*authToken;

- (NSString *) 		authenticate;
- (NSString *) 		authenticateWithEmail:(NSString *)email
							     password:(NSString *)password;
- (void) 			authenticateWithHandler:(HttpRequestCompletedHandler)handler;
- (void) 			clearCredentials;
- (void) 			createTransactionsWithDate:(NSString *)created
							 			amount:(long)amount
						   			  merchant:(NSString *)merchant
							           handler:(HttpRequestCompletedHandler)handler;
- (void) 			getTransactionsWithHandler:(HttpRequestCompletedHandler)handler;
- (NSDictionary *)	httpGetRequestWithUrl:(NSString *)url;
- (NSDictionary *)	httpPostRequestWithUrl:(NSString *)url params:(NSDictionary *)params;
+ (ExpensifyAPI *)	sharedInstance;
- (void) 			showJsonErrorWarning:(NSInteger)jsonCode;
- (void) 			showJsonInvalidWarning;
- (void) 			showNetworkErrorWarning:(NSInteger)httpStatusCode;


@end
