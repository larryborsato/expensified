//
//  Constants.h
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#ifndef Expensified_Constants_h
#define Expensified_Constants_h

static const NSString	*kProductName	= @"Expensified";

static const NSUInteger	kHttpStatusCodeOK	= 200;

static const CGFloat kKeyboardAnimationDuration	= 0.3;
static const CGFloat kMinumumScrollFraction		= 0.2;
static const CGFloat kMaximumScrollFraction		= 0.8;
static const CGFloat kPortraitKeyboardHeight	= 216;


#endif
