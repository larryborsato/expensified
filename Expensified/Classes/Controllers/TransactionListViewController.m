//
//  TransactionListViewController.m
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import "TransactionListViewController.h"
#import	"CreateViewController.h"
#import "TransactionListView.h"
#import "ExpensifyAPI.h"
#import "Constants.h"
#import "SVProgressHUD.h"

@implementation TransactionListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	[[UIBarButtonItem appearance] setTintColor:[UIColor blackColor]];
	
	self.view.backgroundColor = [UIColor whiteColor];
	
	self.navigationItem.title = (NSString *)kProductName;
	
	self.transactionListView = [[TransactionListView alloc] initWithFrame:self.view.frame];
	self.transactionListView.delegate = self;
	[self.view addSubview:self.transactionListView];
	
	[self showSignOutButton];
	[self showCreateTransactionButton];
	[self refresh];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
		
}


/**
 *	Create signout button on left side of navbar
 */
- (void) showSignOutButton
{
	self.buttonSignOut = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Sign Out", @"Text of sign out button")
														  style:UIBarButtonItemStylePlain
														 target:self
														 action:@selector(signOut)];
	self.navigationItem.leftBarButtonItem = self.buttonSignOut;
}


/**
 *	Sign the user out
 */
- (void) signOut
{
	[ExpensifyAPI sharedInstance].authToken = nil;
	[ExpensifyAPI sharedInstance].email 	= nil;
	[ExpensifyAPI sharedInstance].password 	= nil;
	[self.navigationController popViewControllerAnimated:YES];
}


/**
 *	Show the CreateTransaction button on the right side of the nav bar
 */
- (void) showCreateTransactionButton
{
	SEL selector = NSSelectorFromString(@"createTransaction");
	self.buttonCreate = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
																	  target:self
																	  action:selector];
	self.navigationItem.rightBarButtonItem = self.buttonCreate;
}


/**
 *	Refresh the list of transactions
 */
- (void)refresh
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		[self getTransactions];
		[self.transactionListView stopRefresh];
	});
}


/**
 *	Bring up a form to create a new transaction
 */
- (void)createTransaction
{
	CreateViewController *createViewController = [[CreateViewController alloc] initWithNibName:@"CreateViewController" bundle:nil];
	createViewController.navigationItem.hidesBackButton = YES;
	createViewController.delegate = self;
	[self.navigationController pushViewController:createViewController animated:YES];
}


/**
 *	Get the list of transactions
 */
- (void) getTransactions
{
	dispatch_async(dispatch_get_main_queue(),^ {
		[SVProgressHUD showWithStatus:NSLocalizedString(@"Getting Transactions", @"getting transactions")];
	});
	[[ExpensifyAPI sharedInstance] getTransactionsWithHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
														  dispatch_async(dispatch_get_main_queue(),^ {
														    [SVProgressHUD dismiss];
														  });
														  if ( ((NSHTTPURLResponse*)response).statusCode == kHttpStatusCodeOK )
														  {
															  [self processJsonResponse:data];
														  }
														  else
														  {
															  [[ExpensifyAPI sharedInstance] showNetworkErrorWarning:((NSHTTPURLResponse*)response).statusCode];
														  }
													  }];
}


/**
 *	Handle the returned json data
 *
 *	@param	NSData*	data	the returned json data
 */
- (void)processJsonResponse:(NSData *)data
{
	NSError	*jsonError = nil;
	NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
														 options:NSJSONReadingMutableContainers
														   error:&jsonError];
	if ( !jsonError )
	{
		NSInteger jsonCode = [json[kExpensifyKeyJsonCode] integerValue];
		switch ( jsonCode )
		{
			case kExpensifyStatusOK:
			{
				self.transactionListView.transactions = [json objectForKey:kExpensifyKeyTransactionList];
				dispatch_async(dispatch_get_main_queue(),^ {
					[self.transactionListView removeDeletedTransactions];
					[self.transactionListView.tableView reloadData];
				});
				break;
			}
			case kExpensifyStatusTokenExpired:
			case kExpensifyStatusMalformedToken:
			{
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					[[ExpensifyAPI sharedInstance] authenticate];
					[self getTransactions];
				});
				break;
			}
			default:
			{
				[[ExpensifyAPI sharedInstance] showJsonErrorWarning:jsonCode];
				break;
			}
		}
	}
	else
		[[ExpensifyAPI sharedInstance] showJsonInvalidWarning];
}



@end
