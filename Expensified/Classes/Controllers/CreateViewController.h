//
//  CreateViewController.h
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateViewController : UIViewController <UITextFieldDelegate>

@property	id					delegate;
@property	NSString			*authToken;
@property	UIDatePicker		*datePicker;
@property	UIBarButtonItem		*buttonCancel;
@property	UIBarButtonItem		*buttonSave;
@property	UIToolbar			*toolBar;
@property	UIToolbar			*amountInputAccessoryView;
@property	UIToolbar			*dateInputAccessoryView;
@property	UIAlertView			*alertAskBeforeLeaving;
@property	NSNumberFormatter	*currencyFormatter;

@end
