//
//  LoginViewController.h
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransactionListViewController.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property					id			delegate;
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic)	IBOutlet 	UIButton	*buttonSignIn;
@property					NSString	*authToken;
@property					TransactionListViewController	*transactionListViewController;


- (BOOL) 	isEmailAddressValid;
- (BOOL) 	isPasswordValid;
- (IBAction)signIn:(id)sender;
- (BOOL) 	validateCredentials;


@end
