//
//  LoginViewController.m
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import "LoginViewController.h"
#import "TransactionListViewController.h"
#import "ExpensifyAPI.h"
#import "Constants.h"
#import "SVProgressHUD.h"

@interface LoginViewController ()

@end

@implementation LoginViewController


- (id)init
{
	self = [super initWithNibName:@"LoginViewController" bundle:nil];
	return self;
}


- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle
{
	return [self init];
}


- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.transactionListViewController = [[TransactionListViewController alloc] init];
	self.transactionListViewController.navigationItem.hidesBackButton = YES;

	[self setupTextFields];
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	[self.navigationController setNavigationBarHidden:YES animated:NO];
	self.textFieldEmail.text    = @"";
	self.textFieldPassword.text = @"";
}


- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	[self.navigationController setNavigationBarHidden:NO animated:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


/**
 *	Initially set up the text fields
 */
- (void) setupTextFields
{	self.textFieldEmail.delegate 	= self;
	self.textFieldEmail.returnKeyType = UIReturnKeyNext;
	self.textFieldPassword.delegate	= self;
	
	[self.textFieldEmail becomeFirstResponder];
}


/**
 *	Peform the sign in action
 */
- (IBAction)signIn:(id)sender
{
	if ( ![self validateCredentials] )
	{
		return;
	}
	
	self.authToken = [[ExpensifyAPI sharedInstance] authenticateWithEmail:self.textFieldEmail.text
															     password:self.textFieldPassword.text];

	if ( self.authToken )
	{
		if ( [self isFirstLogin] )
			[self showWelcomeMessage];
		
		[self.navigationController pushViewController:self.transactionListViewController animated:YES];
	}
	else
		[self clearCredentials];
}



/**
 *	Check to see if this is the first time the user has used the app
 *
 *	@return	BOOL	YES if first time, NO if not
 */
- (BOOL) isFirstLogin
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL beenThereDoneThat = [defaults boolForKey:@"BeenThereDoneThat"];
	if ( !beenThereDoneThat )
	{
		[defaults setBool:YES forKey:@"BeenThereDoneThat"];
		[defaults synchronize];
	}
	return beenThereDoneThat ? NO : YES;
}


/**
 *	Show the first time user welcome message
 */
- (void) showWelcomeMessage
{
	dispatch_async(dispatch_get_main_queue(),^ {
		NSString *message = @"Thank you for choosing Expensified. We recognize that you have a choice in expenses and we appreciate your patronage.";
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Welcome", @"Welcome")
															message:NSLocalizedString(message, @"the message")
														   delegate:self
												  cancelButtonTitle:NSLocalizedString(@"OK", "Cancel button")
												  otherButtonTitles:nil];
		[alertView show];
	});
}


#pragma mark - TextField validation methods

/**
 *	Clear credentials
 */
- (void) clearCredentials
{
	self.textFieldEmail.text 	= @"";
	self.textFieldPassword.text	= @"";
}



/**
 *	Validate entered credentials
 *
 *	@return	BOOL	YES if successful, NO if not
 */
- (BOOL) validateCredentials
{
	if ( [self isEmailAddressValid]  &&
		 [self isPasswordValid] )
	{
		return YES;
	}
	else
	{
		[self showWarning:@"You have entered an invalid email/password combination. Please try again."];
		[self clearCredentials];
		[self.textFieldEmail becomeFirstResponder];
		return NO;
	}
}


/**
 *	Check for valid email address
 *
 *	@return	BOOL	YES if successful, NO if not
 */
- (BOOL) isEmailAddressValid
{	
	if([self.textFieldEmail.text length]==0){
		return NO;
	}
 
	NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
 
	NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
	NSUInteger regExMatches = [regEx numberOfMatchesInString:self.textFieldEmail.text
													 options:0
													   range:NSMakeRange(0, [self.textFieldEmail.text length])];
 
	if (regExMatches == 0) {
		return NO;
	} else {
		return YES;
	}
}


/**
 *	Check for valid password
 *
 *	@return	BOOL	YES if successful, NO if not
 */
- (BOOL) isPasswordValid
{
	//	Existence is all we need right now
	return ( [self.textFieldPassword.text length] > 0 ) ? YES : NO;
}



/**
 *	Show a warning message
 *
 *	@param	NSString*	message		the message to show
 */
- (void)showWarning:(NSString *)message
{
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"Warning")
														message:NSLocalizedString(message, @"the message")
													   delegate:self
											  cancelButtonTitle:NSLocalizedString(@"OK", "Cancel button")
											  otherButtonTitles:nil];
	[alertView show];
}


#pragma mark - TextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	
	if ( textField == self.textFieldEmail )
	{
		[self.textFieldPassword becomeFirstResponder];
	}
	else if ( textField == self.textFieldPassword )
	{
		dispatch_async(dispatch_get_main_queue(),^ {
			[self signIn:nil];
		});
	}
	
	return YES;
}


#pragma mark - Keyboard handling

- (void) addKeyboardObserver
{
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillShow)
												 name:UIKeyboardWillShowNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillHide)
												 name:UIKeyboardWillHideNotification
											   object:nil];
}


- (void) removeKeyboardObserver
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardWillShowNotification
												  object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardWillHideNotification
												  object:nil];
}


-(void)keyboardWillShow
{
	// Animate the current view out of the way
	if (self.view.frame.origin.y >= 0)
	{
		[self setViewMovedUp:YES];
	}
	else if (self.view.frame.origin.y < 0)
	{
		[self setViewMovedUp:NO];
	}
}


-(void)keyboardWillHide {
	if (self.view.frame.origin.y >= 0)
	{
		[self setViewMovedUp:YES];
	}
	else if (self.view.frame.origin.y < 0)
	{
		[self setViewMovedUp:NO];
	}
}


/**
 *	Move the keyboard as required
 *
 *	@param	BOOL	YES if keyboard should be moved up, NO if down
 */
-(void)setViewMovedUp:(BOOL)movedUp
{
	CGFloat	kKeyboardOffset = 80;
	CGRect rect = self.view.frame;
	if (movedUp)
	{
		// 1. move the view's origin up so that the text field that will be hidden come above the keyboard
		// 2. increase the size of the view so that the area behind the keyboard is covered up.
		rect.origin.y -= kKeyboardOffset;
		rect.size.height += kKeyboardOffset;
	}
	else
	{
		// revert back to the normal state.
		rect.origin.y += kKeyboardOffset;
		rect.size.height -= kKeyboardOffset;
	}
	[UIView animateWithDuration:kKeyboardAnimationDuration animations:^{
		self.view.frame = rect;
	}];
}







@end
