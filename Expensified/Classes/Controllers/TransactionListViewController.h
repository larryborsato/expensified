//
//  TransactionListViewController.h
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransactionListView.h"

@interface TransactionListViewController : UIViewController

@property	TransactionListView	*transactionListView;
@property	NSString			*authToken;
@property	NSString			*email;
@property	NSString			*password;
@property	UIBarButtonItem		*buttonCreate;
@property	UIBarButtonItem		*buttonSignOut;


@end
