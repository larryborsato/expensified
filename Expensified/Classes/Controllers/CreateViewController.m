//
//  CreateViewController.m
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import "CreateViewController.h"
#import "ExpensifyAPI.h"
#import "Constants.h"

@interface CreateViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textFieldMerchant;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAmount;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDate;

@end

@implementation CreateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.navigationItem.title = NSLocalizedString(@"Create", "create title");
	
	[self showCancelButton];
	[self createSaveButton];
	[self showSaveButton:YES];
	
	[self setupTextFields];

	[[UIBarButtonItem appearance] setTintColor:[UIColor blackColor]];
	
	self.currencyFormatter = [[NSNumberFormatter alloc] init];
	[self.currencyFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
}


- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	//[self askBeforeLeaving];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


/**
 *	Setup the initial values for the text fields
 */
- (void) setupTextFields
{
	self.textFieldDate.delegate		= self;
	self.textFieldMerchant.delegate	= self;
	self.textFieldAmount.delegate	= self;
	self.textFieldAmount.keyboardType = UIKeyboardTypeDecimalPad;
	
	self.textFieldAmount.inputAccessoryView = [self createAmountInputAccessoryView];
	
	self.textFieldDate.text = [self formatDate:[NSDate date]];
	[self createDatePicker];
	[self.textFieldDate setInputView:self.datePicker];
	self.textFieldDate.inputAccessoryView = [self createDateInputAccessoryView];
	
}


/**
 *	Create signout button on left side of navbar
 */
- (void) showCancelButton
{
	self.buttonCancel = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", @"Text of cancel button")
													     style:UIBarButtonItemStylePlain
														target:self
														action:@selector(cancel)];
	self.navigationItem.leftBarButtonItem = self.buttonCancel;
}


/**
 *	Sign the user out
 */
- (void) cancel
{
	[self askBeforeLeaving];
}


/**
 *	Show a warning message before cancelling
 *
 *	@param	NSString*	message		the message to show
 */
- (void)askBeforeLeaving
{
	self.alertAskBeforeLeaving = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"Warning")
															message:NSLocalizedString(@"Discard changes?", @"the message")
													   	   delegate:self
												  cancelButtonTitle:NSLocalizedString(@"Yes", "Cancel button")
											  	  otherButtonTitles:NSLocalizedString(@"No", "Stay here button"), nil];
	[self.alertAskBeforeLeaving show];
}


/**
 *	Handle the response to Ask Before Leaving
 *
 *	@param	UIAlertView		the alert view
 *	@param	NSInteger		the index of the clcked button
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	[alertView dismissWithClickedButtonIndex:buttonIndex animated:NO];
	if ( self.alertAskBeforeLeaving && buttonIndex==0 )
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
}


/**
 *	Create the Save button for the right side of the nav bar
 */
- (void) createSaveButton
{
	SEL selector = NSSelectorFromString(@"save");
	self.buttonSave = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
																	target:self
																	action:selector];
}


/**
 *	Show the Save button on the right side of the nav bar
 *
 *	@param	BOOL	YES to show, NO to hide
 */
- (void) showSaveButton:(BOOL)show
{
	if ( show )
		self.navigationItem.rightBarButtonItem = self.buttonSave;
	else
		self.navigationItem.rightBarButtonItem = nil;
}


/**
 *	Enable the save button
 *
 *	@param	BOOL	enable	YES to enable, NO to disable
 */
- (void) enableSaveButton:(BOOL)enable
{
	self.buttonSave.enabled = enable;
}


/**
 *	Create the date picker to be used to pick dates
 */
- (void) createDatePicker
{
	CGRect frame = self.view.frame;
	frame.size.height /= 2;
	frame.origin.y = self.view.frame.size.height - frame.size.height;
	self.datePicker = [[UIDatePicker alloc] initWithFrame:frame];
	self.datePicker.minimumDate 	= [NSDate dateWithTimeIntervalSinceReferenceDate:0];
	self.datePicker.datePickerMode 	= UIDatePickerModeDate;
}


/**
 *	Format a date in YYYY-MM-DD format
 *
 *	@return	NSString*	the formatted date
 */
- (NSString *)formatDate:(NSDate *)date
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	formatter.dateFormat = @"yyyy-MM-dd";
	return [formatter stringFromDate:date];
}


/**
 *	Save the entered data in a new transaction
 */
- (void) save
{
	if ( ![self validateFields] )
	{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", @"Warning")
															message:NSLocalizedString(@"You are missing required information. Please fix it and try again.", @"the message")
														   delegate:self
												  cancelButtonTitle:NSLocalizedString(@"OK", "Cancel button")
												  otherButtonTitles:nil];
		[alertView show];
		return;
	}
	
	NSString *strAmount = [self.textFieldAmount.text stringByReplacingOccurrencesOfString:@"." withString:@""];
	long amount = [strAmount integerValue];
	//	An API issue means that we need to flip the sign of the amount
	amount *= -1;
	[[ExpensifyAPI sharedInstance] createTransactionsWithDate:self.textFieldDate.text
													   amount:amount
													 merchant:self.textFieldMerchant.text
													  handler:^(NSURLResponse *response, NSData *data, NSError *error) {
													  if ( ((NSHTTPURLResponse *)response).statusCode == kHttpStatusCodeOK )
													  {
														  [self processJsonResponse:data];
													  }
													  else
													  {
														  [[ExpensifyAPI sharedInstance] showNetworkErrorWarning:((NSHTTPURLResponse*)response).statusCode];
													  }
												  }];
}


/**
 *	Handle the returned json data
 *
 *	@param	NSData*	data	the returned json data
 */
- (void)processJsonResponse:(NSData *)data
{
	NSError	*jsonError = nil;
	NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
														 options:NSJSONReadingMutableContainers
														   error:&jsonError];
	if ( !jsonError )
	{
		NSInteger jsonCode = [json[kExpensifyKeyJsonCode] integerValue];
		switch ( jsonCode )
		{
			case kExpensifyStatusOK:
			{
				SEL selector = NSSelectorFromString(@"refresh");
				if ( [self.delegate respondsToSelector:selector] )
					[self.delegate performSelector:selector withObject:nil afterDelay:0];
				
				dispatch_async(dispatch_get_main_queue(),^ {
					[self.navigationController popViewControllerAnimated:YES];
				});
				break;
			}
			case kExpensifyStatusTokenExpired:
			case kExpensifyStatusMalformedToken:
			{
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					[[ExpensifyAPI sharedInstance] authenticate];
					[self save];
				});
				break;
			}
			default:
			{
				[[ExpensifyAPI sharedInstance] showJsonErrorWarning:jsonCode];
				break;
			}
		}
	}
	else
		[[ExpensifyAPI sharedInstance] showJsonInvalidWarning];
}


#pragma mark - Input accessory views and handling

/**
 *	Create the input accessory view used when entering amounts
 *
 *	@return	UIToolbar*		the input accessory view
 */
- (UIToolbar *) createAmountInputAccessoryView
{
	self.amountInputAccessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 32)];
	self.amountInputAccessoryView.tintColor = [UIColor darkGrayColor];
	
	NSMutableArray *items = [NSMutableArray array];
	UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Flip Sign", @"Flip Sign button")
											  style:UIBarButtonItemStylePlain
											 target:self
											 action:NSSelectorFromString(@"flipAmountSign")];
	[items addObject:button];

	button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	[items addObject:button];
	
	button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"Done button")
											  style:UIBarButtonItemStylePlain
											 target:self
											 action:NSSelectorFromString(@"doneAmountInput")];
	[items addObject:button];
	
	self.amountInputAccessoryView.items = items;
	return self.amountInputAccessoryView;
}


/**
 *	Create the input accessory view used when entering dates
 *
 *	@return	UIToolbar*		the input accessory view
 */
- (UIToolbar *) createDateInputAccessoryView
{
	self.dateInputAccessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 32)];
	self.dateInputAccessoryView.tintColor = [UIColor darkGrayColor];
	
	NSMutableArray *items = [NSMutableArray array];
	UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	[items addObject:button];
	
	button = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"Done button")
											  style:UIBarButtonItemStylePlain
											 target:self
											 action:NSSelectorFromString(@"doneDateInput")];
	[items addObject:button];
	
	self.dateInputAccessoryView.items = items;
	return self.dateInputAccessoryView;
}


/**
 *	Flip the sign on an amount
 */
- (void) flipAmountSign
{
	if ( [self.textFieldAmount.text hasPrefix:@"-"] )
		self.textFieldAmount.text = [self.textFieldAmount.text substringFromIndex:1];
	else
		self.textFieldAmount.text = [NSString stringWithFormat:@"-%@", self.textFieldAmount.text];
}


/**
 *	Amount entry is done
 */
- (void) doneAmountInput
{
	[self.textFieldAmount resignFirstResponder];
}


/**
 *	Date entry is done
 */
- (void) doneDateInput
{
	[self.textFieldDate resignFirstResponder];
	self.textFieldDate.text = [self formatDate:self.datePicker.date];
}


#pragma mark - TextField delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	
	if ( textField == self.textFieldMerchant )
	{
		[self.textFieldAmount becomeFirstResponder];
	}
	else if ( textField == self.textFieldAmount )
	{
		[self.textFieldDate becomeFirstResponder];
	}
	else if ( textField == self.textFieldDate )
	{
	}
	return YES;
}


- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if ( textField == self.textFieldAmount )
	{
		return [self validateCurrency:textField range:(NSRange)range	replacementString:string];
	}
	else if ( textField == self.textFieldDate )
	{
		return NO;
	}
	return YES;
}


/**
 *	Ensure that only valid currency amounts can be entered
 *
 *	@param	UITextField*	the textfield to look at
 *	@param	NSRange			the range of the text
 *	@param	NSString*		the string to replace it with
 *
 *	@return	BOOL
 */
- (BOOL)validateCurrency:(UITextField*)textField range:(NSRange)range replacementString:(NSString*)string
{
	// Update the string in the text input
	NSMutableString* currentString = [NSMutableString stringWithString:textField.text];
	[currentString replaceCharactersInRange:range withString:string];
	// Strip out the decimal separator
	[currentString replaceOccurrencesOfString:@"." withString:@""
									  options:NSLiteralSearch range:NSMakeRange(0, [currentString length])];
	// Generate a new string for the text input
	int currentValue = [currentString intValue];
	NSString* format = [NSString stringWithFormat:@"%%.%df", 2];
	double minorUnitsPerMajor = pow(10, 2);
	NSString* newString = [NSString stringWithFormat:format, currentValue/minorUnitsPerMajor];
	textField.text = newString;
	return NO;
}


/**
 *	Validate the entered data
 *
 *	@return	BOOL	YES if valid data, NO if not
 */
- (BOOL) validateFields
{
	if ( [self.textFieldMerchant.text length] > 0 &&
		 [self.textFieldAmount.text  length] > 0  &&
		 [self isDateValid:self.textFieldDate.text] )
		return YES;
	else
		return NO;
}


/**
 *	Check if date is valid YYYY-MM-DD format
 *
 *	@param	NSString*	the entered date
 *
 *	@return	BOOL	YES if valid date, NO if not
 */
- (BOOL) isDateValid:(NSString *)dateEntered
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"yyyy-MM-dd";
	NSDate *date = [dateFormatter dateFromString:dateEntered];
	
	return date ? YES : NO;
}


@end
