//
//  TransactionListView.m
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import "TransactionListView.h"
#import "TransactionTableViewCell.h"
#import "ExpensifyAPI.h"
#import "Constants.h"

@implementation TransactionListView

static NSString *CellIdentifier = @"Cell";

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		//self.opaque = 0;
		self.backgroundColor = [UIColor clearColor];
		
		CGRect r = frame;
		self.tableView 							= [[UITableView alloc] initWithFrame:r style:UITableViewStylePlain];
		self.tableView.dataSource 				= self;
		self.tableView.delegate 				= self;
		self.tableView.userInteractionEnabled 	= YES;
		self.tableView.autoresizesSubviews 		= YES;
		self.tableView.autoresizingMask 		= (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
		self.tableView.separatorStyle 			= UITableViewCellSeparatorStyleSingleLine;
		self.tableView.scrollEnabled 			= YES;
		self.tableView.allowsSelection          = NO;
		self.tableView.rowHeight                = 80;
		
		[self.tableView registerNib:[UINib nibWithNibName:@"TransactionTableViewCell" bundle:nil]
			   forCellReuseIdentifier:CellIdentifier];
		
		[self addSubview:self.tableView];
		
		self.currencyFormatter = [[NSNumberFormatter alloc] init];
		[self.currencyFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
		//	Uncomment the next line to display currency country
		//[self.currencyFormatter setCurrencyCode:(NSString *)kExpensifyCurrencyUSD];
		[self createRefreshControl];
	}
	return self;
}


#pragma mark - Pull to Refresh methods

/**
 *	Create the "Pull to Refresh" control
 */
- (void) createRefreshControl
{
	SEL selector = NSSelectorFromString(@"refresh");
	self.refreshControl = [[UIRefreshControl alloc] init];
	self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Pull to Refresh", "pull")];
	[self.refreshControl addTarget:self action:selector forControlEvents:UIControlEventValueChanged];
	[self.tableView addSubview:self.refreshControl];
}


/**
 *	Stop the refreshing and hide the control
 */
- (void)stopRefresh

{
	dispatch_async(dispatch_get_main_queue(),^ {
		[self.refreshControl endRefreshing];
	});
}


/**
 *	Refresh the table view
 */
- (void) refresh
{
	SEL selector = NSSelectorFromString(@"refresh");
	if ( [self.delegate respondsToSelector:selector] )
		[self.delegate performSelector:selector withObject:nil afterDelay:0];
}


#pragma mark - Transaction list processing methods

/**
 *	Removed deleted items from the list of transactions
 */
- (void) removeDeletedTransactions
{
	NSArray *tempTransactions = [[NSArray alloc] initWithArray:self.transactions];
	[self.transactions removeAllObjects];
	for ( NSDictionary *transaction in tempTransactions )
	{
		NSString *reportID = (NSString *)transaction[kExpensifyKeyReportID];
		if ( [reportID integerValue] != -1 )
		{
			[self.transactions addObject:transaction];
		}
	}
	self.transactions = [[NSMutableArray alloc] initWithArray:tempTransactions];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.transactions count];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return @"";
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	long row = [indexPath row];
	
	TransactionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier
																	 forIndexPath:indexPath];

	NSDictionary *transaction = self.transactions[row];
	
	NSString *created  = (NSString *)transaction[kExpensifyKeyCreated];
	NSString *merchant = (NSString *)transaction[kExpensifyKeyMerchant];
	NSString *category = (NSString *)transaction[kExpensifyKeyCategory];
	NSNumber *nAmt     = (NSNumber *)transaction[kExpensifyKeyAmount];

	cell.labelDate.text 	= created  ? created  : @"";
	cell.labelMerchant.text = merchant ? merchant : @"";
	cell.labelCategory.text = category ? category : @"";
	
	float amount =  nAmt ? ((float)[nAmt longValue] / 100.0) : 0.00;
	cell.labelAmount.text	= [self.currencyFormatter stringFromNumber:[NSNumber numberWithFloat:amount]];
	
	return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return NO;
}

@end
