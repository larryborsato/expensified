//
//  TransactionListView.h
//  Expensified
//
//  Created by Larry Borsato on 2015-02-18.
//  Copyright (c) 2015 alchemii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionListView : UIView <UITableViewDataSource, UITableViewDelegate>

@property	id					delegate;
@property	UITableView			*tableView;
@property	NSMutableArray		*transactions;
@property	NSNumberFormatter 	*currencyFormatter;
@property	UIRefreshControl	*refreshControl;

- (void) 	removeDeletedTransactions;
- (void)	stopRefresh;


@end
